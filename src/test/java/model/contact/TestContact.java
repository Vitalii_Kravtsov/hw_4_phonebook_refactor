package model.contact;

import org.junit.Test;

import static org.junit.Assert.*;


public class TestContact {

    @Test
    public void testValidation() {

        assertThrows(IllegalArgumentException.class, () -> new Contact(ContactType.PHONE, "phone number"));
        assertThrows(IllegalArgumentException.class, () -> new Contact(ContactType.PHONE, "123456789"));
        assertThrows(IllegalArgumentException.class, () -> new Contact(ContactType.PHONE, "+380681234"));

        assertThrows(IllegalArgumentException.class, () -> new Contact(ContactType.EMAIL, "email"));
        assertThrows(IllegalArgumentException.class, () -> new Contact(ContactType.EMAIL, "@gmail.com"));
        assertThrows(IllegalArgumentException.class, () -> new Contact(ContactType.EMAIL, "test@com"));
        assertThrows(IllegalArgumentException.class, () -> new Contact(ContactType.EMAIL, "test@mail."));
        assertThrows(IllegalArgumentException.class, () -> new Contact(ContactType.EMAIL, "ivan..ivanov@hotmail.com"));
        assertThrows(IllegalArgumentException.class, () -> new Contact(ContactType.EMAIL, "ivan.ivanov@gmail.com.ua"));
        assertThrows(IllegalArgumentException.class, () -> new Contact(ContactType.EMAIL, "alex.p.gmail.com"));
        assertThrows(IllegalArgumentException.class, () -> new Contact(ContactType.EMAIL, "іванов@gmail.com"));

    }

    @Test
    public void testToString() {

        assertEquals("Phone: +380931234567", new Contact(ContactType.PHONE, "+380931234567").toString());
        assertEquals("Phone: 380671112111", new Contact(ContactType.PHONE, "380671112111").toString());
        assertEquals("Phone: 0960000000", new Contact(ContactType.PHONE, "0960000000").toString());

        assertEquals("E-Mail: test@example.com", new Contact(ContactType.EMAIL, "test@example.com").toString());
        assertEquals("E-Mail: mike.x.2022@gmail.com", new Contact(ContactType.EMAIL, "mike.x.2022@gmail.com").toString());

    }

}
