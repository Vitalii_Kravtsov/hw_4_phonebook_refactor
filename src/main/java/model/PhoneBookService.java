package model;

import model.contact.Contact;
import model.contact.ContactType;

import java.util.List;


public interface PhoneBookService {

    void add(Person person);

    List<Person> sort();

    List<Person> filter(ContactType type);

    List<Person> searchByName(String pattern);

    List<Person> searchByContact(String pattern);

    boolean delete(String value);

    boolean exists(Contact contact);

}
