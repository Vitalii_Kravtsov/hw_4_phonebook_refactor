package ui;

import model.Person;
import model.contact.ContactType;

import java.util.List;


public class Display {

    public void displayContactTypeChoice() {

        System.out.println("Выберите тип контакта");

        for (int i = 0; i < ContactType.values().length; i++) {
            System.out.printf("\t%d. %s%n", i + 1, ContactType.values()[i]);
        }

        System.out.println("\t0. Завершить добавление контактов\n");

    }

    public void displayPersons(List<Person> persons) {

        System.out.println();

        if (persons.size() != 0)
            persons.forEach(person -> System.out.println(person + "\n"));
        else System.out.println("Нет контактов\n");

    }

}
