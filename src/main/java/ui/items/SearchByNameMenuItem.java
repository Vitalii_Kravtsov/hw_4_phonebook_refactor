package ui.items;

import lombok.RequiredArgsConstructor;
import model.PhoneBookService;
import ui.Display;

import java.util.Scanner;


@RequiredArgsConstructor
public class SearchByNameMenuItem implements MenuItem {

    private final Scanner scanner;
    private final Display display;
    private final PhoneBookService phoneBookService;

    @Override
    public boolean isFinal() {
        return false;
    }

    @Override
    public String getMessage() {
        return "Поиск по имени (части имени)";
    }

    @Override
    public void run() {

        System.out.print("Введите имя (часть имени): ");

        this.display.displayPersons(this.phoneBookService.searchByName(this.scanner.next()));

    }

}
