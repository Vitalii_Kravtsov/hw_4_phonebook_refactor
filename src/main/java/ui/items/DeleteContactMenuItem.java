package ui.items;

import lombok.RequiredArgsConstructor;
import model.PhoneBookService;
import ui.Display;

import java.util.Scanner;


@RequiredArgsConstructor
public class DeleteContactMenuItem implements MenuItem {

    private final Scanner scanner;
    private final PhoneBookService phoneBookService;

    @Override
    public boolean isFinal() {
        return false;
    }

    @Override
    public String getMessage() {
        return "Удаление контакта по значению";
    }

    @Override
    public void run() {

        System.out.print("Введите значение контакта: ");

        if (this.phoneBookService.delete(this.scanner.next()))
            System.out.println("Контакт успешно удален\n");
        else System.out.println("Нет контакта с указанным значением\n");

    }

}
