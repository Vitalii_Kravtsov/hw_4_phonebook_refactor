package ui.items;

import lombok.RequiredArgsConstructor;
import model.PhoneBookService;
import ui.Display;


@RequiredArgsConstructor
public class DisplayPhoneBookMenuItem implements MenuItem {

    private final Display display;
    private final PhoneBookService phoneBookService;

    @Override
    public boolean isFinal() {
        return false;
    }

    @Override
    public String getMessage() {
        return "Просмотреть контакты с сортировкой по фамилии";
    }

    @Override
    public void run() {
        this.display.displayPersons(this.phoneBookService.sort());
    }

}
