package ui.items;

import lombok.RequiredArgsConstructor;
import model.PhoneBookService;
import model.contact.ContactType;
import ui.Display;


@RequiredArgsConstructor
public class DisplayPhonesMenuItem implements MenuItem {

    private final Display display;
    private final PhoneBookService phoneBookService;

    @Override
    public boolean isFinal() {
        return false;
    }

    @Override
    public String getMessage() {
        return "Посмотреть только телефоны";
    }

    @Override
    public void run() {
        this.display.displayPersons(this.phoneBookService.filter(ContactType.PHONE));
    }

}
