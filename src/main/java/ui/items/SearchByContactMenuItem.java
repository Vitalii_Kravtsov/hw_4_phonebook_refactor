package ui.items;

import lombok.RequiredArgsConstructor;
import model.PhoneBookService;
import ui.Display;

import java.util.Scanner;


@RequiredArgsConstructor
public class SearchByContactMenuItem implements MenuItem {

    private final Scanner scanner;
    private final Display display;
    private final PhoneBookService phoneBookService;

    @Override
    public boolean isFinal() {
        return false;
    }

    @Override
    public String getMessage() {
        return "Поиск по началу контакта";
    }

    @Override
    public void run() {

        System.out.print("Введите начало контакта: ");

        this.display.displayPersons(this.phoneBookService.searchByContact(this.scanner.next()));

    }

}
